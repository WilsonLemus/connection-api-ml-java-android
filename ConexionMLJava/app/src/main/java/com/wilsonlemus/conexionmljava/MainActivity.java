package com.wilsonlemus.conexionmljava;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    private EditText etBuscar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();

        etBuscar = findViewById(R.id.etBuscar);
    }

    public void onSearchProducto(View v){
        String text = etBuscar.getText().toString();
        if(!text.equals("")){
            ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
            if(networkInfo != null && networkInfo.isConnected()){
                Intent next = new Intent(getApplicationContext(), ResultActivity.class);
                next.putExtra("text", text);
                startActivity(next);
            }else {
                Toast.makeText(getApplicationContext(), "Revisa la conexión a internet", Toast.LENGTH_LONG).show();
            }
        }else{
            Toast.makeText(getApplicationContext(), "Debe ingresar un texto", Toast.LENGTH_SHORT).show();
        }
    }
}