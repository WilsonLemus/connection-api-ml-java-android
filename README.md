# Conexion de APP Android con API de Mercado Libre con Java Nativo

Esta es una aplicacion realizada en Android Studio con lenguaje JAVA, que realiza una conexión con la API de Mercado Libre.
Puede descargar el [APK Debug](app-debug.apk) para realizar su instalacion.

# Contenido del Proyecto

Las carpetas creadas para el funcionamiento del proyecto son:

- **_res/drawable_**: Contiene las imagenes propias de la aplicacion como el logo de la pantalla inicial y una imagen gif para la carga de imagenes externas.
- **_res/layout_**: Contiene los documentos XML de las pantallas que muestra la aplicacion.
    - _activity_main_: es la pantalla inicial de la aplicacion.
    - _activity_result_: es la pantalla donde se muestran los resultados de la busqueda.
    - _activity_detail_: es la pantalla donde se muestra un producto especifico seleccionado en la pantalla anterior.
    - _activity_item_: es la estructura que forma cada elemento del resultado de la busqueda, este se agrega al ListView de la actividad activity_resul.
- **_java/com.wilsonlemus.conexionmljava_**: Contiene los documentos Java de las actividades y documentos logicos:
    - _MainActivity_: Controlador del activity_main 
    - _ResultActivity: Controlador del activity_result
    - _DetailActivity: Controlador del activity_detail
    - _Product_: Clase creada para almacenar los datos del un producto devuelto por el servicio.
    - _Adaptador_: Clase creada para construir los cada elemento que se muestra en el ListView de Result
    - _Conexion_: Es la clase que se encargar en realizar la respectiva peticion _get_ a la Api de Mercado Libre, con una consulta especifica

![Screenshots](Screenshots/estructura.png)

# Funcionalidad
La aplicacion consta de 3 pantallas consecutivas donde cada pantalla depende de la anterior.

![Screenshots](Screenshots/Pantalla_1.png) ![Screenshots](Screenshots/Pantalla_2.png) ![Screenshots](Screenshots/Pantalla_3.png)

### Pantalla Campo de Busqueda
En esta pantalla se realiza la verificacion si hay o no acceso a internet. Al no encontrar encontrar acceso a internet se muestra un mensaje inidcando que no puede acceder a internet. Esto se realiza por medio de las librerias **android.net.ConnectivityManager** y **android.net.NetworkInfo**

Esta verificacion se realiza en el metodo _onSearchProducto_, si existe conexion a internet 
```java
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
public void onSearchProducto(View v){
    String text = etBuscar.getText().toString();
    if(!text.equals("")){
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if(networkInfo != null && networkInfo.isConnected()){
            Intent next = new Intent(getApplicationContext(), ResultActivity.class);
            next.putExtra("text", text);
            startActivity(next);
        }else {
            Toast.makeText(getApplicationContext(), "Revisa la conexión a internet", Toast.LENGTH_LONG).show();
        }
    }else{
        Toast.makeText(getApplicationContext(), "Debe ingresar un texto", Toast.LENGTH_SHORT).show();
    }
}
```

### Visualizacion de resultados de la busqueda
En esta pantalla se recibe el texto ingresado en el campo de la busqueda, se construye al URL del resvicio y se llama la funcion _getApiData_ de la clse **Conexion**, que recibe como parametros: El contexto donde se mostraran los resultados, el ListView de la pantalla y la URL final para realizar el consumo.
Es importante implementar la libreria **implementation 'com.mcxiaoke.volley:library:1.0.19'** en el documento **build.gradle**. Esta libreria es la encargada de realizar la conexion.

Una vez responde el servicio se crea un nuevo objeto tipo _Product_ y se mapean los datos del JSONObject devuelto. Este objeto se agrega a un ArrayList para su envio al adaptador.
```java
public void getApiData(Context context, ListView listView, String url) {
    RequestQueue res = Volley.newRequestQueue(context);
    StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
        @Override
        public void onResponse(String response) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                JSONArray jsonArray = jsonObject.getJSONArray("results");
                String nombre, estado, ciudad, imagen;
                int valor;
                for(int i=0; i < jsonArray.length(); i++){
                    JSONObject element = jsonArray.getJSONObject(i);
                    nombre = element.getString("title");
                    valor = element.getInt("price");
                    estado = element.getJSONObject("address").getString("state_name");
                    ciudad = element.getJSONObject("address").getString("city_name");
                    imagen = "https://" + element.getString("thumbnail").split("/")[2] + "/" + element.getString("thumbnail").split("/")[3];
                    Product product = new Product(nombre, valor, estado, ciudad, imagen);
                    productList.add(product);
                }
                adaptador = new Adaptador(context, productList);
                listView.setAdapter(adaptador);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            Toast.makeText(context, "Error al conectarse a Mercado Libre", Toast.LENGTH_SHORT).show();
        }
    });
    res.add(stringRequest);
}
```

### Adaptador del ListView
En este documento se construye cada elemento del ListView, el cual consta de una imagen, un texto para el valor del producto y un texto para la descripcion del producto.

Esta clase tiene como atributos en Context y ArrayList<Product> y extiendo a _BaseAdapter_ en cual incluye los metodos _getCount_, _getItem_ y _getView_ entre los mas importantes.

```java
@Override
public View getView(int position, View convertView, ViewGroup parent) {
    Product product = (Product) getItem(position);

    convertView = LayoutInflater.from(context).inflate(R.layout.item, null);

    ImageView ivImage = convertView.findViewById(R.id.ivImage);
    Uri uri = Uri.parse(product.imagen);
    String url = product.getImagen();

    RequestOptions options = new RequestOptions()
            .centerCrop()
            .placeholder(R.drawable.load)
            .error(R.drawable.error)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .priority(Priority.HIGH);
    Glide.with(context)
            .load(url)
            .apply(options)
            .centerCrop()
            .into(ivImage);

    TextView tvPrice = convertView.findViewById(R.id.tvPrice);
    TextView tvTitle = convertView.findViewById(R.id.tvTitle);

    String valor = String.format("%,d", product.getValor());
    tvPrice.setText("$ " + valor);
    tvTitle.setText(product.getNombre());

    return convertView;
}
```

Para poder realizar la llamada y muestra de las imagenes del producto, es necesario implementar la libreria Glide **implementation 'com.github.bumptech.glide:glide:4.12.0'** en el documento **build.gradle** e implentar los metodos **RequestOptions** y **Glide**

### Detalle del Producto
Al realizar tap en algun elemento se direcciona a la pantalla de detalle (activity_detail) enviando como parametro un objeto tipo Product y es tratado en DetailActivity para mostrar la informacion del objeto.

```java
private void initial() {
        tvTitle = findViewById(R.id.tvTitle);
        tvLocation = findViewById(R.id.tvLocation);
        tvPrice = findViewById(R.id.tvPrice);
        ivImage = findViewById(R.id.ivImageDetail);

        tvTitle.setText(product.nombre);

        tvLocation.setText(product.ciudad + " - " + product.estado);
        String valor = String.format("%,d", product.valor);
        tvPrice.setText("$ " + valor);

        String url = product.imagen;

        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.load)
                .error(R.drawable.error)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH);
        Glide.with(getApplicationContext())
                .load(url)
                .apply(options)
                .centerCrop()
                .into(ivImage);
    }
```
Con esta clase termina la implementacion de un servicio _get_ hacia Mercado Libre.
