package com.wilsonlemus.conexionmljava;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

public class DetailActivity extends AppCompatActivity {

    TextView tvTitle, tvLocation, tvPrice;
    ImageView ivImage;
    Product product;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        Bundle bundle = getIntent().getExtras();
        product = (Product) bundle.getSerializable("product");

        initial();
    }

    private void initial() {
        tvTitle = findViewById(R.id.tvTitle);
        tvLocation = findViewById(R.id.tvLocation);
        tvPrice = findViewById(R.id.tvPrice);
        ivImage = findViewById(R.id.ivImageDetail);

        tvTitle.setText(product.nombre);

        tvLocation.setText(product.ciudad + " - " + product.estado);
        String valor = String.format("%,d", product.valor);
        tvPrice.setText("$ " + valor);

        String url = product.imagen;

        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.load)
                .error(R.drawable.error)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH);
        Glide.with(getApplicationContext())
                .load(url)
                .apply(options)
                .centerCrop()
                .into(ivImage);
    }
}