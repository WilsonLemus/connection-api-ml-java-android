package com.wilsonlemus.conexionmljava;

import android.content.Context;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Conexion {

    ArrayList<Product> productList = new ArrayList<>();
    Adaptador adaptador;

    public void getApiData(Context context, ListView listView, String url) {
        RequestQueue res = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("results");
                    String nombre, estado, ciudad, imagen;
                    int valor;
                    for(int i=0; i < jsonArray.length(); i++){
                        JSONObject element = jsonArray.getJSONObject(i);
                        nombre = element.getString("title");
                        valor = element.getInt("price");
                        estado = element.getJSONObject("address").getString("state_name");
                        ciudad = element.getJSONObject("address").getString("city_name");
                        imagen = "https://" + element.getString("thumbnail").split("/")[2] + "/" + element.getString("thumbnail").split("/")[3];
                        Product product = new Product(nombre, valor, estado, ciudad, imagen);
                        productList.add(product);
                    }
                    adaptador = new Adaptador(context, productList);
                    //ArrayAdapter<String> adaptador = new ArrayAdapter<Product>(getApplicationContext(), R.layout.support_simple_spinner_dropdown_item, );
                    listView.setAdapter(adaptador);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, "Error al conectarse a Mercado Libre", Toast.LENGTH_SHORT).show();
            }
        });

        res.add(stringRequest);
    }
}
