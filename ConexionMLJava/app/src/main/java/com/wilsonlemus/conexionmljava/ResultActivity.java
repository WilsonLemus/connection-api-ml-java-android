package com.wilsonlemus.conexionmljava;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

public class ResultActivity extends AppCompatActivity {

    String url = "";
    ArrayList<Product> productList = new ArrayList<>();
    ListView lvResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        Bundle bundle = getIntent().getExtras();
        String text = bundle.getString("text");
        url = "https://api.mercadolibre.com/sites/MCO/search?q=" + text + "#json";

        lvResult = findViewById(R.id.lvResult);
        try {
            Conexion conexion = new Conexion();
            conexion.getApiData(this, lvResult, url);
            productList = conexion.productList;
        } catch (Exception e) {
            e.printStackTrace();
        }
        lvResult.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Product pro = productList.get(position);
                Intent next = new Intent(getApplicationContext(), DetailActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("product", pro);
                next.putExtras(bundle);
                startActivity(next);
            }
        });
    }
}