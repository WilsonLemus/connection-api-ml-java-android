package com.wilsonlemus.conexionmljava;

import java.io.Serializable;

public class Product implements Serializable {
    public String nombre;
    public int valor;
    public String estado;
    public String ciudad;
    public String imagen;

    public Product(String nombre, int valor, String estado, String ciudad, String imagen) {
        this.nombre = nombre;
        this.valor = valor;
        this.estado = estado;
        this.ciudad = ciudad;
        this.imagen = imagen;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }
}
