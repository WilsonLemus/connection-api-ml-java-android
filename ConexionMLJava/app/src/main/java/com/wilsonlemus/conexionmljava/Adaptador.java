package com.wilsonlemus.conexionmljava;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

public class Adaptador extends BaseAdapter {

    private Context context;
    private ArrayList<Product> listProduct;

    public Adaptador(Context context, ArrayList<Product> listProduct) {
        this.context = context;
        this.listProduct = listProduct;
    }

    @Override
    public int getCount() {
        return listProduct.size();
    }

    @Override
    public Object getItem(int position) {
        return listProduct.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Product product = (Product) getItem(position);

        convertView = LayoutInflater.from(context).inflate(R.layout.item, null);

        ImageView ivImage = convertView.findViewById(R.id.ivImage);
        Uri uri = Uri.parse(product.imagen);
        String url = product.getImagen();
        //String url = "https://http2.mlstatic.com/" + urlImage;

        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.load)
                .error(R.drawable.error)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH);
        Glide.with(context)
                .load(url)
                .apply(options)
                .centerCrop()
                .into(ivImage);

        TextView tvPrice = convertView.findViewById(R.id.tvPrice);
        TextView tvTitle = convertView.findViewById(R.id.tvTitle);

        String valor = String.format("%,d", product.getValor());
        tvPrice.setText("$ " + valor);
        tvTitle.setText(product.getNombre());

        return convertView;
    }
}
